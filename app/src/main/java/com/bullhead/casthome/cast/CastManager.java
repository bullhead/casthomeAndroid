package com.bullhead.casthome.cast;

import com.bullhead.casthome.CastHome;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastState;

public final class CastManager {
    private static CastManager instance;
    private final CastPlayer castPlayer;
    private CastContext castContext;

    private CastManager() {
        castContext = CastContext.getSharedInstance(CastHome.getContext());
        castPlayer = new CastPlayer(castContext);
    }

    public static CastManager getInstance() {
        if (instance == null) {
            instance = new CastManager();
        }
        return instance;
    }

    public CastPlayer getCastPlayer() {
        return castPlayer;
    }

    public boolean isConnected() {
        return castContext.getCastState() == CastState.CONNECTED;
    }

}
