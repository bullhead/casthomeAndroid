package com.bullhead.casthome.cast;


import androidx.annotation.NonNull;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.framework.*;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;

public class CastPlayer implements SessionManagerListener<Session> {
    private SessionManager sessionManager;

    CastPlayer(CastContext castContext) {
        sessionManager = castContext.getSessionManager();
    }

    public void startListening() {
        sessionManager.addSessionManagerListener(this);

    }


    public void stopListening() {
        try {

            sessionManager.removeSessionManagerListener(this);
        } catch (Exception e) {
            //ignore
        }
    }

    @Override
    public void onSessionStarting(Session castSession) {

    }

    @Override
    public void onSessionStarted(Session castSession, String s) {
        //check if we was playing with
        checkAndPlay();
    }

    @Override
    public void onSessionStartFailed(Session castSession, int i) {

    }

    @Override
    public void onSessionEnding(Session castSession) {

    }

    @Override
    public void onSessionEnded(Session castSession, int i) {

    }

    @Override
    public void onSessionResuming(Session castSession, String s) {

    }

    @Override
    public void onSessionResumed(Session castSession, boolean b) {
    }

    @Override
    public void onSessionResumeFailed(Session castSession, int i) {

    }

    @Override
    public void onSessionSuspended(Session castSession, int i) {

    }

    /**
     * Check if we was playing with exo player and stop it and start playing with cast.
     * If not playing then do not do anything.
     */
    private void checkAndPlay() {

    }

    public void play(@NonNull final String url) {

        MediaTrack mediaTrack = new MediaTrack.Builder(1, MediaTrack.TYPE_AUDIO)
                .setContentId(url)
                .setName("Hindi")
                .build();
        ArrayList<MediaTrack> mediaTracks = new ArrayList<>();
        mediaTracks.add(mediaTrack);

        MediaInfo mediaInfo = new MediaInfo.Builder(url)
                .setStreamType(MediaInfo.STREAM_TYPE_NONE)
                .setContentType("video/*")
                .setMetadata(trackInfoToMeta())
                .setMediaTracks(mediaTracks)
                .build();
        MediaLoadOptions mediaLoadOptions = new MediaLoadOptions.Builder()
                .setAutoplay(true).build();
        CastSession castSession = sessionManager.getCurrentCastSession();
        if (castSession != null) {
            final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
            if (remoteMediaClient != null) {

                remoteMediaClient.load(mediaInfo, mediaLoadOptions)
                        .setResultCallback(new ResultCallbacks<RemoteMediaClient.MediaChannelResult>() {
                            @Override
                            public void onSuccess(@NonNull RemoteMediaClient.MediaChannelResult mediaChannelResult) {

                            }

                            @Override
                            public void onFailure(@NonNull Status status) {
                            }
                        });
            }
        }

    }

    private MediaMetadata trackInfoToMeta() {
        MediaMetadata mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MUSIC_TRACK);
        mediaMetadata.putString(MediaMetadata.KEY_TITLE, "Movie");

        return mediaMetadata;
    }




}
