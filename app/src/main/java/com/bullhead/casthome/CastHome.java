package com.bullhead.casthome;

import android.app.Application;

public class CastHome extends Application {
    private static CastHome context;

    public static CastHome getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }
}
